<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REGEX</title>
</head>
<body>


    <?php
    // comment récupérer le contenu d'un bloc div

    $html = '<div class="test">Lorem ipsum dolor sit amet.</div>';
    preg_match('/<div class="test">(.*?)<\/div>/s', $html, $match);

    
    // Extraire le contenu, y compris l’élément parent (balises):
    echo $match[0];

    // Extraire le contenu entre les balises:
    echo $match[1];
    ?>

</body>
</html>